using Avalonia.Controls;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;

namespace BassoonJukebox.Views
{
    public class JukeboxView : UserControl
    {
        private string _previousPath = "";

        public JukeboxView()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }

        /// <summary>
        /// When the Folder button of the Jukebox widget is clicked, this will launch a dialog to
        /// select a file.  It will then talkt to the JukeboxViewModel to try and load that file
        /// as audio.
        /// </summary>
        public async void OpenFile(object sender, RoutedEventArgs args)
        {
            // Open a file dialog to get some playable music
            OpenFileDialog dialog = new OpenFileDialog()
            {
                Title = "Open Music File",
                InitialFileName = _previousPath,
            };
            string[] files = await dialog.ShowAsync(window());

            // `files` may be `null` if no files were selected
            if (files?.Length > 0)
                TryLoadAudio(files[0]);
        }

        private Window window() => (Window)this.VisualRoot;

        private ViewModels.JukeboxViewModel viewModel() => (ViewModels.JukeboxViewModel)this.DataContext;

        /// <summary>
        /// Calling lets someone load an audio file.
        /// </summary>
        /// <param name="filepath">file to try to load</param>
        /// <returns>true if successful, false otherwise</returns>
        public bool TryLoadAudio(string filepath)
        {
            // If the file loaded, next time we'd like to open the dialog again in that location
            bool ok = viewModel().TryLoadAudio(filepath);
            if (ok)
                _previousPath = viewModel().LoadedFilePath;

            return ok;
        }
    }
}