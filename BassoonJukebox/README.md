Bassoon Jukebox
===============

Plays music!

Note, before doing `dotnet run` here, please read this section of the Bassoon README:
https://gitlab.com/define-private-public/Bassoon/-/blob/b74b95a52af8ef69bb6d04bcdf7f4f6c3488608a/README.rst#a-note-about-running-on-linux-and-os-x


Showcases:
- Buttons with Images
  - NOTE: This was written with Avalonia `0.9.7`, which doesn't support SVG images at the moment.
          So PNGs are used for now.
- Inverted Sliders
  - Same as above note, had to do it with a bit of a workaround hack since the normal inversion
    property flag isn't supported in Avalonia at the moment.  Issue has been filed:
    https://github.com/AvaloniaUI/Avalonia/issues/3617
- Buttons containing text and images
  - Mimics `GtkFileChooserButton` button
- Using an `IConverter` to change out an image using it's file path in a binding to `Source`
  - `BitmapValueConverter` was taken from AvalonStudio
- How in retrive commandline arguments via the `IClassicDesktopStyleApplicationLifetime.Startup`
  event.  In thie example, we use it to load a file via the command line.

Not fully ready:
- Can't drag slider to change playback location:
  `Slider.Thumb` events dont' seem to work yet, filed a bug: https://github.com/AvaloniaUI/Avalonia/issues/4210