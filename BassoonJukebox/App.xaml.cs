using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using BassoonJukebox.ViewModels;
using BassoonJukebox.Views;

namespace BassoonJukebox
{
    public class App : Application
    {
        private MainWindow _window;
        private JukeboxView _jukebox;

        public override void Initialize()
        {
            AvaloniaXamlLoader.Load(this);
        }

        public override void OnFrameworkInitializationCompleted()
        {
            if (ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                _window = new MainWindow
                {
                    DataContext = new MainWindowViewModel(),
                };

                desktop.MainWindow = _window;
                desktop.Startup += _onAppStart;

                _jukebox = _window.Find<JukeboxView>("Jukebox");
                _jukebox.DataContext = new JukeboxViewModel();
            }

            base.OnFrameworkInitializationCompleted();
        }

        /// <summary>
        /// This funciton is used to capture the command line arguments and try to load an initial file.
        /// </summary>
        private void _onAppStart(object sender, ControlledApplicationLifetimeStartupEventArgs args)
        {
            // Make sure we have at least one thing
            if (args.Args.Length < 1)
                return;

            // Attempt to load the initial
            string initial = args.Args[0].Trim();
            _jukebox.TryLoadAudio(initial);
        }
    }
}