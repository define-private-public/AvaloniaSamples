using System;
using System.IO;
using System.Reactive;
using Avalonia.Threading;
using ReactiveUI;
using Bassoon;
using libsndfileSharp;
using PortAudioSharp;

namespace BassoonJukebox.ViewModels
{
    /// <summary>
    /// View model for the Jukebox widget.
    ///
    /// This is where all of the widget's logic exists.  Controling playback, what should be shown, etc.
    /// </summary>
    public class JukeboxViewModel : ViewModelBase
    {
        /// <summary>
        /// The playback slider works on an integer basis, so to give it enough precision, we give it a value of 1,000,000
        /// </summary>
        public const int PlaybackSliderValueMax = 1000000;

        // Icons (swapable)
        public const string PlayIconPNG = "avares://BassoonJukebox/Assets/play.png";
        public const string PauseIconPNG = "avares://BassoonJukebox/Assets/pause.png";

        #region UI variables
        // These store the state for the `Reactive Properties` seciton below
        private string _statusMsg = "";
        private string _volumeDisplay = "";
        private double _volumeSliderValue = 0;
        private int _playbackSliderValue = 0;
        private string _playbackTimeDisplay = "";
        private string _loadedFilename = "";
        private bool _isPlaying = false;
        private string _playButtonIcon = "";
        private bool _movingPlaybackSlider = false;
        #endregion // UI variables

        /// <summary>
        /// Used as a samsies guard to prevent reloading files
        /// </summary>
        public string LoadedFilePath { get; private set; } = "";

        /// <summary>
        /// Actual audio playback object
        /// </summary>
        internal Sound _audio = null;

        // Bounded button commands
        public ReactiveCommand<Unit, Unit> PlayPress { get; }
        public ReactiveCommand<Unit, Unit> RewindPress { get; }

        // Used to monitor audio playback, and update the playback slider
        private DispatcherTimer _playbackTimer = new DispatcherTimer();

        public JukeboxViewModel()
        {
            // Add in some defaults
            StatusMsg = "Hello, ready to play music.";
            LoadedFilename = "(Click me to open some music)";
            Volume = 90;
            PlaybackSliderValue = 0;
            PlaybackTimeDisplay = "00:00:00";
            PlayButtonIcon = PlayIconPNG;

            // Make sure users can only press playback control buttons if there is an audio file loaded
            IObservable<bool> canAdjustPlaybackState = this.WhenAny(
                model => model.Audio,
                x => (x.Value != null)
            );
            PlayPress = ReactiveCommand.Create(_onPlayClicked, canAdjustPlaybackState);
            RewindPress = ReactiveCommand.Create(_onRewindClicked, canAdjustPlaybackState);

            // Timers to monitor status of playback
            //   Note that it might be a bit inefficent to have this timer constantly running for the live of this UI.
            //   It would be better to start/pause the timer if the user pressed Play/Pause, or loaded up another song
            _playbackTimer.Tick += new EventHandler(_checkPlayback);
            _playbackTimer.Interval = TimeSpan.FromSeconds(0.05);        // Check every 1/20 of a second
            _playbackTimer.Start();
        }

        ~JukeboxViewModel()
        {
            // On close, cleanup the timer
            _playbackTimer.Stop();
        }

        #region Reactive Properties
        /// <summary>
        /// Message the shows on the lower part of the widget
        /// </summary>
        public string StatusMsg
        {
            get => _statusMsg;
            set => this.RaiseAndSetIfChanged(ref _statusMsg, value);
        }

        // NOTE:
        //  Avalonia 0.9.7 doesn't support the `IsDirectionReversed`, so we have to manually have
        //  to invert the value ourselves.  This way if we want the value at say, 90%, we can
        //  Give this funciton `90`, but it will correct it for the slider's direction.

        /// <summary>
        /// Get the volume, as a value from 0-100 %
        /// </summary>
        public double Volume
        {
            get => 100 - _VolumeSlider;
            set => _VolumeSlider = (100 - value);
        }

        /// <summary>
        /// The "true" volume slider property and funciton.  See the above note for why
        /// </summary>
        internal double _VolumeSlider
        {
            get => _volumeSliderValue;
            set
            {
                this.RaiseAndSetIfChanged(ref _volumeSliderValue, value);

                // Update the GUI
                VolumeDisplay = $"{Volume}%";

                // Update the audio (if we have one)
                if (Audio != null)
                    Audio.Volume = (float)(Volume / 100.0);
            }
        }

        /// <summary>
        /// Note above the slider that shows the volume level as a text message.
        /// </summary>
        public string VolumeDisplay
        {
            get => _volumeDisplay;
            set => this.RaiseAndSetIfChanged(ref _volumeDisplay, value);
        }

        /// <summary>
        /// Integer value of the playback slider.
        /// </summary>
        public int PlaybackSliderValue
        {
            get => _playbackSliderValue;
            set => this.RaiseAndSetIfChanged(ref _playbackSliderValue, value);
        }

        /// <summary>
        /// Label to the left of the playback slider, shows what the current location in the song as a timestamp
        /// </summary>
        public string PlaybackTimeDisplay
        {
            get => _playbackTimeDisplay;
            set => this.RaiseAndSetIfChanged(ref _playbackTimeDisplay, value);
        }

        /// <summary>
        /// Name of the file that's loaded (displayed in the button with the folder icon)
        /// </summary>
        public string LoadedFilename
        {
            get => _loadedFilename;
            set => this.RaiseAndSetIfChanged(ref _loadedFilename, value);
        }

        /// <summary>
        /// Filepath for the icon to sow on the play/pause button
        /// </summary>
        /// <value></value>
        internal string PlayButtonIcon
        {
            get => _playButtonIcon;
            set => this.RaiseAndSetIfChanged(ref _playButtonIcon, value);
        }

        /// <summary>
        /// The Audio needs to be reactively accessable because of the `canAdjustPlaybackState` (see the constructor)
        /// </summary>
        internal Sound Audio
        {
            get => _audio;
            set => this.RaiseAndSetIfChanged(ref _audio, value);
        }
        #endregion // Reactive UI Stuffs

        /// <summary>
        /// Try to load an audio file
        /// </summary>
        /// <param name="path">Audio file to load</param>
        /// <returns>true if a new audio file was loaded, false otherwise</retruns>
        public bool TryLoadAudio(string path)
        {
            bool loadSuccess = false;

            // First see if the full path is the same or not
            if (LoadedFilePath == path)
                return loadSuccess;

            // New audio file! (clean out the old)
            unloadAudio();

            // Load the new (first update the UI)
            LoadedFilePath = path;
            LoadedFilename = Path.GetFileName(LoadedFilePath);

            // Load the new
            string newStatusText = "";
            try
            {
                Audio = new Sound(path);
                Audio.Volume = getNormalizedVolume();
                newStatusText = $"Loaded {LoadedFilename}";
                loadSuccess = true;
            }
            catch (SndFileException ex)
            {
                newStatusText = $"Error decoding {LoadedFilename}";
                Console.WriteLine($"loadAudio() SndFileException: {ex.Message}");
            }
            catch (PortAudioException ex)
            {
                newStatusText = $"Error setting up stream for {LoadedFilename}";
                Console.WriteLine($"loadAudio() PortAudioException: {ex.Message}");
            }

            // Reset the GUI to it's initial state (not playing)
            PlaybackSliderValue = 0;
            PlaybackTimeDisplay = "00:00:00";
            PlayButtonIcon = PlayIconPNG;

            StatusMsg = newStatusText;
            return loadSuccess;
        }

        /// <summary>
        /// Unloads any current audio file (if any)
        /// </summary>
        private void unloadAudio()
        {
            if (Audio != null)
            {
                Audio.Pause();
                Audio.Dispose();
                Audio = null;
            }

            _isPlaying = false;
        }

        /// <summary>
        /// The UI's volume works on the range of [0, 100], we need it at [0.0, 1.0] for Bassoon
        /// </summary>
        private float getNormalizedVolume() => (float)Volume / 100f;

        /// <summary>
        /// When the play/pause butotn is clicked, this will start audio playback (if a file is loaded)
        /// </summary>
        private void _onPlayClicked()
        {
            // Null audio guard
            if (Audio == null)
                return;

            string statusText;

            // Do we play or pause?
            if (_isPlaying)
            {
                Audio.Pause();
                statusText = $"Paused {LoadedFilename}";
                PlayButtonIcon = PlayIconPNG;
            }
            else
            {
                Audio.Play();
                statusText = $"Playing {LoadedFilename}";
                PlayButtonIcon = PauseIconPNG;
            }

            StatusMsg = statusText;

            // Flip the play state
            _isPlaying = !_isPlaying;
        }

        /// <summary>
        /// Called by _playbackTimer's timeout method, this checks the status of playback and updates
        /// UI accordingly.
        /// </summary>
        private void _checkPlayback(object sender, EventArgs e)
        {
            // We need to have an audio file
            if (Audio == null)
                return;

            if (Audio.IsPlaying)
            {
                // adjust the slider if playing (and not scrubbing)
                if (!_movingPlaybackSlider)
                    PlaybackSliderValue = (int)_map(Audio.Cursor, 0, Audio.Duration.TotalSeconds, 0, PlaybackSliderValueMax);

                // Update the time label
                double t = (double)PlaybackSliderValue / (double)PlaybackSliderValueMax;
                PlaybackTimeDisplay = TimeSpan.FromSeconds(t * Audio.Duration.TotalSeconds).ToString(@"hh\:mm\:ss");

                return;
            }
        }

        /// <summary>
        /// When the rewind button is clicked, set playback to the beginning
        /// </summary>
        private void _onRewindClicked()
        {
            if (Audio == null)
                return;

            Audio.Cursor = 0;                   // Reset audio to the start
            PlaybackSliderValue = 0;            // Reset playback slider to beginning
            PlaybackTimeDisplay = "00:00:00";   // Reset label to intial state
        }

        // NOTE: Slider.Thumb's drag events aren't implemnted in Avalonia at the time of writing this
        /*
        public void OnPlaybackSliderDragStarted(object sender, VectorEventArgs e)
        {
            _movingPlaybackSlider = true;
        }

        public void OnPlaybackSliderDragCompleted(object sender, VectorEventArgs e)
        {
            _movingPlaybackSlider = false;
        }
        */

        /// <summary>
        /// Map a value (x) from one range [a, b] to a new range [p, q]
        /// </summary>
        /// <param name="x">value to map</param>
        /// <param name="a">old min</param>
        /// <param name="b">old max</param>
        /// <param name="p">new min</param>
        /// <param name="q">new max</param>
        /// <returns></returns>
        private static double _map(double x, double a, double b, double p, double q) =>
            (x - a) / (b - a) * (q - p) + p;
    }
}
