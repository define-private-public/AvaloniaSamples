using System;
using System.Reactive;
using ReactiveUI;

namespace Calculator.ViewModels
{
    public class CalculatorViewModel : ViewModelBase
    {
        private long _storedValue = 0;
        private MathOp _storedOp = MathOp.NOP;
        private long _inputValue = 0;
        private bool _doingNegativeInput = false;

        private const int _maxInputDigits = 9;
        private int _numDigitsEntered = 0;

        private string _display = "";

        // Bounded button commands
        public ReactiveCommand<int, Unit> NumberPress { get; }
        public ReactiveCommand<MathOp, Unit> MathOpPress { get; }
        public ReactiveCommand<string, Unit> ModifierOpPress { get; }

        public CalculatorViewModel()
        {
            // Don't let users input too many digits
            IObservable<bool> canInputNumbers = this.WhenAnyValue(
                x => x.NumDigitsEntered,
                x => (x < _maxInputDigits)
            );
            NumberPress = ReactiveCommand.Create<int>(_onNumberPress, canInputNumbers);

            // Before being to do some math, need to have some input first (or a stored value)
            IObservable<bool> canDoMath = this.WhenAnyValue(
                x => x.InputValue,
                x => x.StoredValue,
                (iv, sv) => ((iv != 0) || (sv != 0))
            );
            MathOpPress = ReactiveCommand.Create<MathOp>(_onMathOpPress, canDoMath);

            // Modifier ops can be pressed at any time
            ModifierOpPress = ReactiveCommand.Create<string>(_onModifierOpPress);

            // Set to 0
            StoredValue = 0;
            InputValue = 0;
            _showNumberOnDisplay(InputValue);
        }

        #region Reactive Notifier Properties
        public int NumDigitsEntered
        {
            get => _numDigitsEntered;
            set => this.RaiseAndSetIfChanged(ref _numDigitsEntered, value);
        }

        public long InputValue
        {
            get => _inputValue;
            set => this.RaiseAndSetIfChanged(ref _inputValue, value);
        }

        public long StoredValue
        {
            get => _storedValue;
            set => this.RaiseAndSetIfChanged(ref _storedValue, value);
        }

        public string Display
        {
            get => _display;
            set => this.RaiseAndSetIfChanged(ref _display, value);
        }
        #endregion // Reactive Notifier Properties

        #region Command Handlers
        /// <summary>
        /// When one of the number buttons is pressed, this will tack it onto the end
        /// </summary>
        private void _onNumberPress(int num)
        {
            // Edge case for where we have nothing entered yet, but the user keeps pressing `0`
            if ((num == 0) && (NumDigitsEntered == 0))
                return;

            // If there is a stored value (that just was displayed, i.e, the process is done), then they start inputing a new number
            //   (it should clear the stored value too)
            if ((StoredValue != 0) && (_storedOp == MathOp.NOP))
                StoredValue = 0;

            // Increase
            NumDigitsEntered++;
            InputValue *= 10;

            if (_doingNegativeInput)
                InputValue -= num;
            else
                InputValue += num;

            _showNumberOnDisplay(InputValue);
        }

        /// <summary>
        /// When one of the math operation buttons (e.g.null "+", "-", "x", "/") is pressed, this will notify of a math operation
        /// </summary>
        private void _onMathOpPress(MathOp op)
        {
            // If there is a currently stored value & stored operation, do it?
            if (StoredValue != 0)
            {
                long oldStored = StoredValue;
                switch (_storedOp)
                {
                    case MathOp.Add:
                        StoredValue += InputValue;
                        break;

                    case MathOp.Sub:
                        StoredValue -= InputValue;
                        break;

                    case MathOp.Mul:
                        StoredValue *= InputValue;
                        break;

                    case MathOp.Div:
                        // If they try to divide by zero, be a little cute
                        if (InputValue == 0)
                            Display = "[haha, nice try]";
                        else
                            StoredValue /= InputValue;
                        break;
                }

                // If the value changed, update it
                if (oldStored != StoredValue)
                    _showNumberOnDisplay(StoredValue);
            }
            else
            {
                // Queuing up an operation, store the currently input value, and show the op on screen
                StoredValue = InputValue;
                _showMathOpOnDisplay(op);
            }

            // Store the operation for the next go
            if (op != MathOp.Eval)
                _storedOp = op;
            else
                _storedOp = MathOp.NOP;

            // Always reset input state
            _resetInputState();
        }

        /// <summary>
        /// When the `C` or `+/-` buttons are pressed, this will modify the state of the input (or clear it)
        /// </summary>
        private void _onModifierOpPress(string op)
        {
            switch (op)
            {
                case "neg":
                    // Negate
                    if ((InputValue == 0) && ((StoredValue == 0) || ((StoredValue != 0) && (_storedOp != MathOp.NOP))))
                    {
                        _doingNegativeInput = !_doingNegativeInput;
                        Display = "-0";     // Note: this is no such thing as "negative zero" in math.  It's only here as a visual aid
                    }
                    else if (InputValue != 0)
                    {
                        // Negate the input value (if there is one)
                        InputValue *= -1;
                        _doingNegativeInput = !_doingNegativeInput;
                        _showNumberOnDisplay(InputValue);
                    }
                    else
                    {
                        StoredValue *= -1;      // If not, then negate the stored value
                        _showNumberOnDisplay(StoredValue);
                    }

                    break;

                case "clr":
                    // Clear the calculator state
                    StoredValue = 0;
                    _storedOp = MathOp.NOP;
                    _resetInputState();
                    _showNumberOnDisplay(0);
                    break;
            }
        }
        #endregion // Command Handlers

        private void _resetInputState()
        {
            _doingNegativeInput = false;
            InputValue = 0;
            NumDigitsEntered = 0;
        }

        #region Display functions
        private void _showNumberOnDisplay(long num) =>
            Display = $"{num:n0}";

        private void _showMathOpOnDisplay(MathOp op)
        {
            switch (op)
            {
                case MathOp.Add:
                    Display = "+";
                    break;

                case MathOp.Sub:
                    Display = "-";
                    break;

                case MathOp.Mul:
                    Display = "X";
                    break;

                case MathOp.Div:
                    Display = "/";
                    break;

                default:
                    _showNumberOnDisplay(StoredValue);
                    break;
            }
        }
        #endregion // Display functions
    }
}
