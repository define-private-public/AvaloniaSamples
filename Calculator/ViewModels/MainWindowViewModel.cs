﻿using System;
using System.Collections.Generic;
using System.Text;
using ReactiveUI;

namespace Calculator.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        private ViewModelBase content;

        public CalculatorViewModel Calc { get; }

        public MainWindowViewModel()
        {
            Content = Calc = new CalculatorViewModel();
        }

        public ViewModelBase Content
        {
            get => content;
            private set => this.RaiseAndSetIfChanged(ref content, value);
        }
    }
}
