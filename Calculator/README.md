Calculator
==========

A very simple calculator.

Showcases:
- Basic styling for a class
- Grid layout
- Reactive UI
- Using a custom Enum in XAML, and have it passed into a `Command`
- Simple Linear Gradient

TODO:
- [ ] Add hotkeys
