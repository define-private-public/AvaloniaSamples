namespace Calculator
{
    public enum MathOp
    {
        // Do nothing
        NOP,

        // Actual opertaions
        Add,
        Sub,
        Mul,
        Div,

        // Does the equation
        Eval
    };
}
