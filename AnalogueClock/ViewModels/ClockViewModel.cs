using System;
using System.Reactive;
using System.Timers;
using ReactiveUI;

namespace AnalogueClock.ViewModels
{
    public class ClockViewModel : ViewModelBase
    {
        private DateTime _currentTime;

        private string _digitalTimeDisplay = "";

        private Timer _updateTimer = new Timer(1000.0 / 60.0);  // Updates 60x a second


        public ClockViewModel()
        {
            DigitalTimeDisplay = "00:00:00 AM";

            // Set up the auto update timer
            _updateTimer.Elapsed += _onUpdateTimerTimeout;
            _updateTimer.AutoReset = true;
            _updateTimer.Enabled = true;
        }

        #region Reactive Notifier Properties
        public DateTime CurrentTime
        {
            get => _currentTime;
            set => this.RaiseAndSetIfChanged(ref _currentTime, value);
        }

        public string DigitalTimeDisplay
        {
            get => _digitalTimeDisplay;
            set => this.RaiseAndSetIfChanged(ref _digitalTimeDisplay, value);
        }
        #endregion // Reactive Notifier Properties

        private void _onUpdateTimerTimeout(object sender, ElapsedEventArgs args)
        {
            DateTime now = args.SignalTime;
            CurrentTime = now;
            DigitalTimeDisplay = now.ToString("HH:mm:ss tt");
        }
    }
}
