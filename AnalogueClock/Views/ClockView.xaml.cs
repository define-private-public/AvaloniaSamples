using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace AnalogueClock.Views
{
    public class ClockView : UserControl
    {
        public ClockView()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}