Analogue Clock
==============

A round clock w/ a face

Showcases:
- Custom drawing using SkiaSharp
- Animation (i.e. updating multiple times per second)
- Binding a `DirectProperty` onto a custom drawn control
