using System;
using System.Diagnostics.CodeAnalysis;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Threading;
using Avalonia.Media;
using Avalonia.Platform;
using Avalonia.Skia;
using Avalonia.Rendering.SceneGraph;
using SkiaSharp;

namespace AnalogueClock.Widgets
{
    // A simple 12-hour clock, hour, minute, and second hand, with fractional second clock
    public class AnalogueClock : Control
    {
        // Add this in makes it so in XAML we can access/bind the `CurrentTime` property.
        #region CurrentTime Property
        public static readonly DirectProperty<AnalogueClock, DateTime> CurrentTimeProperty =
            AvaloniaProperty.RegisterDirect<AnalogueClock, DateTime>(
                nameof(CurrentTime),
                ac => ac.CurrentTime,                   // Getter
                (ac, v) => ac.CurrentTime = v);         // Setter

        private DateTime _currentTime;
        public DateTime CurrentTime
        {
            get => _currentTime;
            set
            {
                _currentTime = value;
                InvalidateVisual();
            }
        }
        #endregion // CurrentTime Property

        public AnalogueClock()
        {
            ClipToBounds = true;
        }

        public override void Render(DrawingContext context)
        {
            // queue up a draw
            context.Custom(new _renderClockDrawOp(new Rect(0, 0, Bounds.Width, Bounds.Height), CurrentTime));
            Dispatcher.UIThread.InvokeAsync(InvalidateVisual, DispatcherPriority.Background);
        }

        private class _renderClockDrawOp : ICustomDrawOperation
        {
            public const float SecondsIn12Hours = 60 * 60 * 12;
            public const float SecondsIn1Hour = 60 * 60;
            private readonly FormattedText _noSkiaError = new FormattedText() { Text = "Current rendering API is not Skia" };

            private readonly float _hourHand;           // Value [0, 1] where the hour hand should be
            private readonly float _minuteHand;         // VAlue [0, 1] where the minute hand should be
            private readonly float _secondHand;         // Value [0, 1] where the second hand should be
            private readonly float _secondFractionHand; // Value [0, 1] where the "fractions of a second" hand should be

            public Rect Bounds { get; }

            // Drawing constants
            private const float _strokeSize = 16;
            private const float _tickWidth = _strokeSize / 2;
            private const float _tickHeight = 24;
            private const float _margin = (_strokeSize / 2) + 1;
            private const float _secondFractionClockRadius = 16;
            private SKPoint _shadowOffset = new SKPoint(4, 8);

            #region Skia Paints
            SKPaint _redFill = new SKPaint
            {
                IsAntialias = true,
                Color = new SKColor(0xFF, 0x33, 0x33),
            };

            SKPaint _whiteFill = new SKPaint
            {
                IsAntialias = true,
                Color = new SKColor(0xFF, 0xFF, 0xFF),
            };

            SKPaint _blackFill = new SKPaint
            {
                IsAntialias = true,
                Color = new SKColor(0x00, 0x00, 0x00),
            };

            SKPaint _blackStroke = new SKPaint
            {
                IsAntialias = true,
                Color = new SKColor(0x00, 0x00, 0x00),
                Style = SKPaintStyle.Stroke,
                StrokeWidth = _strokeSize,
            };

            SKPaint _shadowStroke = new SKPaint
            {
                IsAntialias = true,
                Color = new SKColor(0x99, 0x99, 0x99),
                Style = SKPaintStyle.Stroke,
                StrokeWidth = _strokeSize,
                BlendMode = SKBlendMode.Darken,
            };

            SKPaint _shadowFill = new SKPaint
            {
                IsAntialias = true,
                Color = new SKColor(0x99, 0x99, 0x99),
                StrokeWidth = _strokeSize,
                BlendMode = SKBlendMode.Darken,
            };
            #endregion // Skia Paints


            public _renderClockDrawOp(Rect bounds, DateTime currentTime)
            {
                Bounds = bounds;

                float currentSeconds = (float)currentTime.TimeOfDay.TotalSeconds;
                _hourHand = (currentSeconds % SecondsIn12Hours) / SecondsIn12Hours;
                _minuteHand = (currentSeconds % SecondsIn1Hour) / SecondsIn1Hour;
                _secondHand = (currentSeconds % 60f) / 60f;
                _secondFractionHand = (currentSeconds % 1f) / 1f;
            }

            // Don't need to implement these
            public void Dispose() { }
            public bool Equals([AllowNull] ICustomDrawOperation other) => false;
            public bool HitTest(Point p) => false;

            public void Render(IDrawingContextImpl context)
            {
                // First attempt to get the context
                SKCanvas canvas = (context as ISkiaDrawingContextImpl)?.SkCanvas;
                if (canvas == null)
                {
                    context.DrawText(Brushes.Red, new Point(), _noSkiaError.PlatformImpl);
                    return;
                }

                SKPoint center = Bounds.Center.ToSKPoint();
                float width = (float)Bounds.Width - (2 * _margin);
                float height = (float)Bounds.Height - (2 * _margin);
                float r = System.MathF.Min(width, height) / 2;
                SKPath clockClipPath = new SKPath();
                clockClipPath.AddCircle(0, 0, r);

                // Widget
                canvas.Save();
                canvas.Translate(center);

                // First clock base
                canvas.DrawCircle(0, 0, r, _whiteFill);
                canvas.DrawCircle(0, 0, r, _blackStroke);

                // Draw the shadow (makes it look inset)
                canvas.Save();
                canvas.ClipPath(clockClipPath);
                canvas.DrawCircle(_shadowOffset, r, _shadowStroke);
                canvas.Restore();

                // The innards of the clock
                canvas.Save();

                // Center nob
                canvas.DrawCircle(0, 0, _tickWidth, _blackFill);
                canvas.DrawCircle(_shadowOffset, _tickWidth, _shadowFill);

                // The ticks
                _drawClockTicks(canvas, 12, _tickWidth, _tickHeight, (2 * _margin), r);
                float tickY = r - _tickHeight - (2 * _margin);

                // The hands
                canvas.Save();
                canvas.RotateDegrees(180f);
                _drawClockHand(canvas, _secondHand, _tickWidth / 2f, tickY, _redFill);
                _drawClockHand(canvas, _minuteHand, _tickWidth, (tickY - (2 * _margin)), _blackFill);
                _drawClockHand(canvas, _hourHand, _tickWidth * 1.5f, (tickY - _margin) / 1.5f, _blackFill);
                canvas.Restore();   // Hands

                canvas.Restore();   // Innards

                // The "Fractions of a second" hand/clock
                canvas.Save();
                canvas.Translate(r - _secondFractionClockRadius, r - _secondFractionClockRadius);
                _drawClockTicks(canvas, 10, 1, _secondFractionClockRadius / 4, 1, _secondFractionClockRadius);
                _drawClockHand(canvas, _secondFractionHand, 1.5f, _secondFractionClockRadius * 0.6f, _blackFill, false);
                canvas.Restore();   // Fractions of a second hand

                canvas.Restore();   // Widget
            }

            /// <summary>
            /// Draw's a clock's hand
            /// </summary>
            /// <param name="c">Canvas to render on</param>
            /// <param name="range">Value from [0, 1], of where the hand should go on the clock (e.g `.25` would put it at 3 o'clock, `.5` to 6 o'clock</param>
            /// <param name="width">width of the hand</param>
            /// <param name="height">height of the hand</param>
            /// <param name="clr">color to draw the hand</param>
            private void _drawClockHand(SKCanvas c, float range, float width, float height, SKPaint clr, bool drawShadow=true)
            {
                float halfWidth = width / 2;
                float deg = 360f * range;

                if (drawShadow)
                {
                    // Hand's shadow first
                    c.Save();
                    c.Translate(new SKPoint() {
                        X = -_shadowOffset.X,
                        Y = -_shadowOffset.Y,
                    });
                    c.RotateDegrees(deg);
                    c.DrawRect(-halfWidth, _margin, width, height, _shadowFill);
                    c.Restore();
                }

                // The hand
                c.Save();
                c.RotateDegrees(deg);
                c.DrawRect(-halfWidth, _margin, width, height, clr);
                c.Restore();
            }

            /// <summary>
            /// Draw the ticks for a clock
            /// </summary>
            /// <param name="c">SKCanvas to render on</param>
            /// <param name="numTicks">number of ticks to draw</param>
            /// <param name="tickWidth">width of each tick</param>
            /// <param name="tickHeight">height of each tick</param>
            /// <param name="distFromClockEdge"> How far from the clock's edge the tick should be</param>
            /// <param name="clockRadius">radius of the clock</param>
            private void _drawClockTicks(SKCanvas c, uint numTicks, float tickWidth, float tickHeight, float distFromClockEdge, float clockRadius)
            {
                float tickDeltaDeg = 360f / (float)numTicks;
                float tickY = clockRadius - tickHeight - distFromClockEdge;

                for (uint i = 0; i < numTicks; i++)
                {
                    c.Save();
                    c.RotateDegrees(tickDeltaDeg * i);
                    c.Translate(-tickWidth / 2, tickY);

                    c.DrawRect(0, 0, tickWidth, tickHeight, _blackFill);
                    c.Restore();
                }
            }
        }
    }
}
